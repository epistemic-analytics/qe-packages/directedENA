## directedENA 0.3.0

#### Bugs
  * Fixed centering of projection
  
#### Features
  * Updated network plots

## directedENA 0.2.0

#### Bugs
  * Fixed layering of edges - thicker edges plotting on top of thinner edges

#### Features
  * Parameter `opacity_range` added to `plot.directed.ena.set()` to customize the edge opacity

## directedENA 0.1.0

#### Features 
  * Initial Version
