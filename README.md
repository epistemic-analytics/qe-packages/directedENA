# directedENA


## What is directedENA 

Plots directed epistemic networks.

## Installation

### Install release version from CRAN

[![cran status](https://www.r-pkg.org/badges/version-ago/directedENA)](https://cran.r-project.org/package=directedENA) 
[![cran downloads](https://cranlogs.r-pkg.org/badges/grand-total/directedENA)](https://cranlogs.r-pkg.org/badges/grand-total/directedENA) 
```
Coming Soon
```

### Install development version

[![pipeline status](https://gitlab.com/epistemic-analytics/qe-packages/directedENA/badges/master/pipeline.svg)](https://gitlab.com/epistemic-analytics/qe-packages/directedENA/-/commits/master)
[![coverage report](https://gitlab.com/epistemic-analytics/qe-packages/directedENA/badges/master/coverage.svg)](https://gitlab.com/epistemic-analytics/qe-packages/directedENA/-/commits/master)
```
install.packages("directedENA", repos = c("https://cran.qe-libs.org", "https://cran.rstudio.org"))
```

## Resources
To learn more about ENA, visit the [resources page](http://www.epistemicnetwork.org/resources/).
