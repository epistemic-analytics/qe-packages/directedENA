% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_.R
\name{points_}
\alias{points_}
\title{Title}
\usage{
points_(
  x,
  ...,
  p = NULL,
  model = NULL,
  points = NULL,
  units_as_vectors = TRUE,
  unit_vectors_towards = "response",
  mean_vector_width = 5,
  mean_vector_arrow_size = 0.5,
  unit_vector_width = 3,
  unit_vector_arrow_size = 0.7,
  show_points = TRUE,
  point_shape = "circle",
  dynamic_response_point_size = FALSE,
  show_mean = TRUE,
  mean_value = NULL,
  colors = "#FF0000",
  saturate_vectors_by = 0.2,
  point_position_multiplier = 1,
  distance_scaled_by = function(x) {
     scales::rescale(x, to = c(0.3, 1.5), from =
    c(0, 2))
 },
  with_lines = TRUE
)
}
\arguments{
\item{x}{TBD}

\item{...}{passed to base_plot}

\item{p}{TBD}

\item{model}{TBD}

\item{points}{TBD}

\item{units_as_vectors}{TBD}

\item{unit_vectors_towards}{TBD}

\item{mean_vector_width}{TBD}

\item{mean_vector_arrow_size}{TBD}

\item{unit_vector_width}{TBD}

\item{unit_vector_arrow_size}{TBD}

\item{show_points}{TBD}

\item{point_shape}{TBD}

\item{dynamic_response_point_size}{TBD}

\item{show_mean}{TBD}

\item{mean_value}{TBD}

\item{colors}{TBD}

\item{saturate_vectors_by}{TBD}

\item{point_position_multiplier}{TBD}

\item{distance_scaled_by}{TBD}

\item{with_lines}{TBD}
}
\value{
TBD
}
\description{
Title
}
