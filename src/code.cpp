// [[Rcpp::depends(RcppArmadillo)]]

#include <RcppArmadillo.h>

using namespace Rcpp;

//' Upper Triangle from Vector
//'
//' @title vector to upper triangle
//' @description TBD
//' @param v TBD
//' @export
// [[Rcpp::export]]
arma::rowvec vector_to_ut(arma::mat v) {
  int vL = v.size();
  int vS = ( (vL * (vL + 1)) / 2) - vL;

  arma::rowvec vR2( vS, arma::fill::zeros );
  int s = 0;
  for( int i = 2; i <= vL; i++ ) {
    for (int j = 0; j < i-1; j++ ) {
      vR2[s] = v[j] * v[i-1];
      s++;
    }
  }
  return vR2;
}

//' Acumulate Stanza Window
//'
//' @title accum_stanza_window
//' @name accum_stanza_window
//' @description TBD
//' @param df A dataframe
//' @param windowSize Integer for number of rows in the stanza window
//' @param binary Logical, treat codes as binary or leave as weighted
//' @export
// [[Rcpp::export]]
DataFrame accum_stanza_window(
  DataFrame df,
  float windowSize = 1,
  bool binary = true
) {
  int dfRows = df.nrows();
  int dfCols = df.size();
  int numCoOccurences = dfCols * dfCols;

  arma::mat df_CoOccurred(dfRows, numCoOccurences, arma::fill::zeros);
  arma::mat df_AsMatrix(dfRows, dfCols, arma::fill::zeros);

  for (int i=0; i<dfCols;i++) {
    df_AsMatrix.col(i) = Rcpp::as<arma::vec>(df[i]);
  }

  for(int row = 0; row < dfRows; row++) {
    int earliestRow = 0, lastRow = row;

    if (windowSize == std::numeric_limits<double>::infinity()) {
      earliestRow = 0;
    }
    else if ( windowSize == 0 ) {
      earliestRow = row;
    }
    else if ( row - (windowSize-1) >= 0 ) {
      earliestRow = row - (windowSize - 1);
    }

    arma::mat currRows2 = df_AsMatrix( arma::span(earliestRow, lastRow), arma::span::all );
    arma::mat currRowsSummed = arma::sum(currRows2);
    arma::mat r = df_AsMatrix( lastRow, arma::span::all );
    arma::mat currRowAdj (dfCols, dfCols, arma::fill::zeros);
    currRowAdj.diag() = r;

    arma::mat W (dfCols, dfCols, arma::fill::zeros);


    W = (currRowsSummed.t() * r) - currRowAdj;
    // Rcpp::Rcout << "r" << std::endl;
    // Rcpp::Rcout << r << std::endl;
    // Rcpp::Rcout << "currRows2" << std::endl;
    // Rcpp::Rcout << currRows2 << std::endl;
    // Rcpp::Rcout << "currRowsSummed" << std::endl;
    // Rcpp::Rcout << currRowsSummed << std::endl;
    // Rcpp::Rcout << "currRowAdj" << std::endl;
    // Rcpp::Rcout << currRowAdj << std::endl;
    // Rcpp::Rcout << "W" << std::endl;
    // Rcpp::Rcout << W << std::endl;
    df_CoOccurred.row(row) = W.as_col().t();
  }
  if(binary == true) {
    df_CoOccurred.elem( find(df_CoOccurred > 0) ).ones();
  }

  return wrap(df_CoOccurred);
}

//' Multiobjective, Component by Component, with Ellipsoidal Scaling, for directed ENA
//'
//' @title Multiobjective, Component by Component, with Ellipsoidal Scaling, for directed ENA
//' @description TBD
//' @param line_weights TBD
//' @param points TBD
//' @param numDims TBD
//' @export
// [[Rcpp::export]]
Rcpp::List directed_node_positions(arma::mat line_weights, arma::mat points, int numDims) { //, bool by_column = true) { // = R_NilValue ) {
  int numNodes = ceil(std::sqrt(static_cast<double>(line_weights.n_cols)));

  arma::mat node_weights = arma::mat(line_weights.n_rows, numNodes, arma::fill::zeros); // zc: added an extra column

  int row_count = line_weights.n_rows;
  for (int k = 0; k < row_count; k++) {
    arma::mat currAdj = line_weights.row(k);

    int z = 0;
    for(int x = 0; x < numNodes; x++) {
      for(int y = 0; y < numNodes; y++) {
        node_weights(k,x) = node_weights(k,x) + currAdj(z);
        // added the following line, zc, 10.29.2021
        node_weights(k,y) = node_weights(k,y) + currAdj(z);
        z = z + 1;
      }
    }
  }

  for (int k = 0; k < row_count; k++) {
    double length = 0;
    for(int i = 0; i < numNodes; i++) {
      length = length + std::abs(node_weights(k,i));
    }
    if(length < 0.0001) {
      length = 0.0001;
    }
    for(int i = 0; i < numNodes; i++) {
      node_weights(k,i) = node_weights(k,i) / length;
    }
  }

  arma::mat ssX = arma::mat(numDims, numNodes, arma::fill::zeros);
  arma::mat ssA = node_weights.t() * node_weights;
  arma::mat ssb;
  for(int i = 0; i < numDims; i++) {
    ssb = node_weights.t() * points.col(i);
    ssX.row(i) = arma::solve(ssA, ssb, arma::solve_opts::equilibrate  ).t();
  }

  arma::mat centroids = (ssX * node_weights.t()).t();

  return Rcpp::List::create(
    _("nodes") = ssX.t(),
    //_("correlations") = compute_difference_correlations(centroids, t),
    _("centroids") = centroids,
    _("weights") = node_weights, // zc: remember that the last column is all 1
    _("points") = points
  );
}

//' Node position optimization with ground and response weights/points added
//'
//' @title Node position optimization with ground and response weights/points added
//' @description TBD
//' @param line_weights TBD
//' @param points TBD
//' @param numDims TBD
//' @export
// [[Rcpp::export]]
Rcpp::List directed_node_positions_with_ground_response_added(arma::mat line_weights, arma::mat points, int numDims) { //, bool by_column = true) { // = R_NilValue ) {
  int numNodes = ceil(std::sqrt(static_cast<double>(line_weights.n_cols)));

  arma::mat node_weights = arma::mat(line_weights.n_rows, numNodes, arma::fill::zeros);

  int row_count = line_weights.n_rows;
  for (int k = 0; k < row_count; k++) {
    arma::mat currAdj = line_weights.row(k);

    int z = 0;
    for(int x = 0; x < numNodes; x++) {
      for(int y = 0; y < numNodes; y++) {
        node_weights(k,x) = node_weights(k,x) + currAdj(z);
        // added the following line, zc, 10.29.2021
        node_weights(k,y) = node_weights(k,y) + currAdj(z);
        z = z + 1;
      }
    }
  }

  for (int k = 0; k < row_count; k++) {
    double length = 0;
    for(int i = 0; i < numNodes; i++) {
      length = length + std::abs(node_weights(k,i));
    }
    if(length < 0.0001) {
      length = 0.0001;
    }
    for(int i = 0; i < numNodes; i++) {
      node_weights(k,i) = node_weights(k,i) / length;
    }
  }
  // the following block is to add ground and response node weights/points
  arma::mat node_weights_added = arma::mat(line_weights.n_rows/2, numNodes, arma::fill::zeros);
  arma::mat points_added = arma::mat(line_weights.n_rows/2, numDims, arma::fill::zeros);

  for(int k=0;k<row_count;k+=2)
  {
    for(int i=0;i<numNodes;i++)
      node_weights_added(k/2,i)=node_weights(k,i)+node_weights(k+1,i);
    for(int i=0;i<numDims;i++)
      points_added(k/2,i)=points(k,i)+points(k+1,i);
  }
  arma::mat ssX = arma::mat(numDims, numNodes, arma::fill::zeros);
  arma::mat ssA = node_weights_added.t() * node_weights_added;
  arma::mat ssb;
  for(int i = 0; i < numDims; i++) {
    ssb = node_weights_added.t() * points_added.col(i);
    ssX.row(i) = arma::solve(ssA, ssb, arma::solve_opts::equilibrate  ).t();
  }

  arma::mat centroids = (ssX * node_weights.t()).t();

  return Rcpp::List::create(
    _("nodes") = ssX.t(),
    //_("correlations") = compute_difference_correlations(centroids, t),
    _("centroids") = centroids,
    _("weights") = node_weights,
    _("points") = points
  );
}

// [[Rcpp::export]]
Rcpp::NumericMatrix center_data_c(arma::mat values) {
  arma::mat centered = values.each_row() - mean(values);
  // arma::mat centered2(values.n_rows, values.n_cols);
  // arma::colvec m = mean(values);
  // for(int i = 0; i < values.n_rows; i++) {
  //   centered2.row(i) = values.row(i) - m;
  // }
  return Rcpp::wrap(centered);
}

/*** R
# dat <- data.table::data.table(
#   Name=c("J","Z"),
#   Day=c(1,1,1,1,1,1,2,2,2,2,2,2),
#   c1=c(1,1,1,1,1,0,0,1,1,0,0,1),
#   c2=c(1,1,1,0,0,1,0,1,0,1,0,0),
#   c3=c(0,0,1,0,1,0,1,0,0,0,1,0),
#   c4=c(1,1,1,0,0,1,0,1,0,1,0,0)
# );
# dat_acc <- dat[, {
#     accum_stanza_window(.SD, windowSize = 2, binary = TRUE)
#   },
#   by = c("Day"),
#   .SDcols = c("c1", "c2", "c3", "c4")
# ]
#
# lines2 <- matrix(c(0,1,0,1,1,0,1,0,0,0,0,1), ncol = 3, dimnames = list(NULL, LETTERS[1:3]))
# accum_stanza_window(lines2, windowSize = 4, binary = FALSE)

# dat <- read.csv(system.file("extdata", "devils_advocate_data.csv", package = "directedENA"))
# code_cols <- colnames(dat)[8:13];
#
# set_resp_rot <- directed_ena(dat,
#   units = c("Devils.Advocate", "Group", "Speaker"),
#   conversations = c("Group", "Round", "Time"),
#   codes = code_cols,
#   binary = TRUE,
#   windowSize = 4,
#   rotation_on = "response"
# )
#
# points = as.matrix(set_resp_rot$points) #[filter_rows, ];
# weights = as.matrix(set_resp_rot$line.weights) #[filter_rows, ];
# positions = directed_node_positions(weights, points, ncol(points))
# positions2 = directed_node_positions_2(weights, points, ncol(points))
#
# rows <- set_resp_rot$points$ENA_DIRECTION == "ground"
# correlations(pts = points[rows, ], cts = positions$centroids[rows, ], direction = "ground")
# correlations(pts = points[rows, ], cts = positions2$centroids[rows, ], direction = "ground")
#
# rows <- set_resp_rot$points$ENA_DIRECTION == "response"
# correlations(pts = points[rows, ], cts = positions$centroids[rows, ], direction = "response")
# correlations(pts = points[rows, ], cts = positions2$centroids[rows, ], direction = "response")
#
# set_grnd_rot <- directed_ena(dat,
#   units = c("Devils.Advocate", "Group", "Speaker"),
#   conversations = c("Group", "Round", "Time"),
#   codes = code_cols,
#   binary = TRUE,
#   windowSize = 4,
#   rotation_on = "ground"
# )
# # filter_rows <- set$line.weights$ENA_DIRECTION == "response"
# points2 = as.matrix(set_grnd_rot$points) #[filter_rows, ];
# weights2 = as.matrix(set_grnd_rot$line.weights) #[filter_rows, ];
# positions21 = directed_node_positions(weights2, points2, ncol(points2))
# positions22 = directed_node_positions_2(weights2, points2, ncol(points2))
#
# rows2 <- set_grnd_rot$points$ENA_DIRECTION == "ground"
# correlations(pts = points2[rows2, ], cts = positions2$centroids[rows2, ], direction = "ground")
#
# rows2 <- set_grnd_rot$points$ENA_DIRECTION == "response"
# correlations(pts = points[rows2, ], cts = positions2$centroids[rows2, ], direction = "response")
#
# microbenchmark::microbenchmark(
#     positions1 = directed_node_positions(weights, points, ncol(points)),
#     positions2 = directed_node_positions_2(weights2, points2, ncol(points2))
# )

print("Done")
*/
